﻿using UnityEngine;
using System.Collections;
using System;

public class Treasure2D : MonoBehaviour {

    public int minimumValue = 10;
    public int maximumValue = 500;

    int treasureValue;

	// Use this for initialization
    void Start()
    {
        System.Random pseudoRandom = new System.Random(DateTime.Now.ToLongTimeString().GetHashCode());
        treasureValue = pseudoRandom.Next(minimumValue, maximumValue);
    }

    public int getTreasureValue()
    {
        return treasureValue;
    }
}
