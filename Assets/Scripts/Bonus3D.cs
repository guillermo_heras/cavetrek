﻿using UnityEngine;
using System.Collections;
using System;

public class Bonus3D : MonoBehaviour {

    public float minimumIntensity = 3f;
    public float maximumIntensity = 6f;
    public float timeLimit = 4f;
    private Light lightComponent;
    private float toIntensity, fromIntensity;
    private float currentTime = 0;
    private bool playerInside = false;

    private Player player;
    public float rechargeMultiplier = 5f;

	// Use this for initialization
	void Start () {
        System.Random pseudoRandom = new System.Random(DateTime.Now.ToLongTimeString().GetHashCode());
        lightComponent = GetComponent<Light>();
        fromIntensity = minimumIntensity + (float)pseudoRandom.NextDouble() * (maximumIntensity-minimumIntensity);
        toIntensity = (Mathf.Abs(minimumIntensity-fromIntensity) > Mathf.Abs(maximumIntensity-fromIntensity))?minimumIntensity:maximumIntensity;
        //player = GameObject.FindObjectOfType<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime;
        lightComponent.intensity = Mathf.Lerp(fromIntensity, toIntensity, currentTime/timeLimit);
        if (currentTime / timeLimit >= 1)
        {
            currentTime = 0;
            fromIntensity = (toIntensity == maximumIntensity)?maximumIntensity:minimumIntensity;
            toIntensity = (fromIntensity == maximumIntensity)?minimumIntensity:maximumIntensity;
        }

        if (playerInside)
        {
            //player.Recharge(lightComponent.intensity * rechargeMultiplier * Time.deltaTime);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerInside = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerInside = false;
        }
    }
}
