﻿using UnityEngine;
using System.Collections;

public class Exit3D : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject.FindObjectOfType<MapGenerator>().EndLevel();
        }
    }
}
