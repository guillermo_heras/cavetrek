﻿using UnityEngine;
using System.Collections;

public class Camera2D : MonoBehaviour {

	public Transform objective;
	private Transform _transform;

	// Use this for initialization
	void Start () {
		_transform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		_transform.position = new Vector3(objective.position.x,objective.position.y,_transform.position.z);
	}
}
