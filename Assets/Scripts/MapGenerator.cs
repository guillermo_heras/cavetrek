﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class MapGenerator : MonoBehaviour {
    public int width;
    public int height;

    [Range(0, 100)]
    public int randomMapFill;

    public string seed;
    public bool useRandomSeed;
    

    public int smoothPasses = 5;
    public int borderSize = 5;
    public int wallThresholdSize = 50;
    public int roomThresholdSize = 50;

    public int minimumPassageRadius = 1;
    public int maximumPassageRadius = 3;

    public float squareSize = 1; 

	public Camera camera2D;
    public Player player3D;
    public Player2D player2D;
	public GameObject roof;

    private Room mainRoom;
    private List<Room> allRooms;

    public int enemyChance, treasureChance, bonusChance, exitChance = 10;
    public int numberEnemies = 3;
    public int numberTreasures = 1;
    public int numberBonuses = 1;
    public int numberExits = 1;

    public int level = 1;
    public Text levelTextUI;

    public Enemy2D[] enemies2D;
	public Enemy3D[] enemies3D;
	public Treasure2D[] treasures2D;
	public Treasure3D[] treasures3D;
	public Bonus2D[] bonuses2D;
	public Bonus3D[] bonuses3D;
	public Exit2D exit2D;
	public Exit3D exit3D;

    public Canvas UICanvas2D;
	public Canvas UICanvas3D;
    public Camera mainCamera;
	public Camera minimapCamera;
	public Light minimapLight;

    public static System.Random pseudoRandom;

    private MeshGenerator meshGenerator;
    int[,] map;

    void Awake()
    {
        if (level == 0)
        {
            //Initial loading scene
            if (useRandomSeed)
            {
                seed = DateTime.Now.ToLongTimeString();
            }

            pseudoRandom = new System.Random(seed.GetHashCode());
        }

    }

    void Start()
    {
        if (level == 0)        
        {
			meshGenerator = GetComponent<MeshGenerator>();
			DontDestroyOnLoad(this);
			DontDestroyOnLoad(player2D);
			DontDestroyOnLoad(player3D);
			DontDestroyOnLoad(UICanvas2D);
			DontDestroyOnLoad(UICanvas3D);
			DontDestroyOnLoad(minimapCamera);
			DontDestroyOnLoad(mainCamera);
			DontDestroyOnLoad(minimapLight);
        }
        else if (level % 2 != 0)  //2D level
        {
            InitializeLevel();
            GenerateMap();
            InitializePlayer();
            InitializeRooms();
        } else {   //3D level
			InitializeLevel3D();
			GenerateMap3D();
			InitializePlayer3D();
			InitializeRooms3D();
		}
    }

    void Update()
    {
		if (level == 0)
			EndLevel();

        if (Input.GetKeyDown(KeyCode.N))
			EndLevel();
		if (Input.GetKeyDown(KeyCode.G))
			GameOver();
			
	}

	void OnLevelWasLoaded()
    {
        Start();
    }

    
    #region Map

    void InitializeLevel()
    {
		numberEnemies = pseudoRandom.Next(1 + (int)Math.Floor(Math.Max(1,Math.Log(2*level))), 3 + (int)Math.Floor(Math.Max(1,Math.Log(2*level))));
		numberTreasures = pseudoRandom.Next(1+(int)Math.Floor(Math.Max(1,Math.Log(level))),  1+(int)Math.Floor(Math.Max(1,Math.Log(2*level))));
		numberBonuses = pseudoRandom.Next(1,  (int)Math.Floor(Math.Max(1,Math.Log(1+level))));
        mainRoom = null;
        if (allRooms != null)
            allRooms.Clear();
    }

	void InitializeLevel3D()
	{
	}

	void GenerateMap3D() {
		MeshGenerator meshGen = GetComponent<MeshGenerator>();
		meshGen.CreateWallMesh();
	}

    void GenerateMap()
    {
        map = new int[width, height];
        RandomFillMap();

        for (int i = 0; i < smoothPasses; i++)
        {
            SmoothMap();
        }

        ProcessMap();

        int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

        for (int x = 0; x < borderedMap.GetLength(0); x++)
        {
            for (int y = 0; y < borderedMap.GetLength(1); y++)
            {
                if (x >= borderSize && x < width + borderSize && y >= borderSize && y < height + borderSize)
                {
                    borderedMap[x, y] = map[x - borderSize, y - borderSize];
                }
                else
                {
                    borderedMap[x, y] = 1;
                }
            }
        }

        MeshGenerator meshGen = GetComponent<MeshGenerator>();
        meshGen.GenerateMesh(borderedMap, squareSize);
    }

    void ProcessMap()
    {
        List<List<Coord>> wallRegions = GetRegions(1);

        foreach (List<Coord> wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (Coord tile in wallRegion)
                {
                    map[tile.tileX, tile.tileY] = 0;
                }
            }
        }

        List<List<Coord>> roomRegions = GetRegions(0);
        allRooms = new List<Room>();

        foreach (List<Coord> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Coord tile in roomRegion)
                {
                    map[tile.tileX, tile.tileY] = 1;
                }
            }
            else
            {
                allRooms.Add(new Room(roomRegion, map));
            }
        }

        allRooms.Sort();
        allRooms[0].isMainRoom = true;        
        allRooms[0].isAccessibleFromMainRoom = true;

        mainRoom = allRooms[0];

        ConnectClosestRooms(allRooms);
    }

    void ConnectClosestRooms(List<Room> currentRooms, bool forceAccessibilityFromMainRoom = false)
    {
        List<Room> roomListA = new List<Room>();
        List<Room> roomListB = new List<Room>();

        if (forceAccessibilityFromMainRoom)
        {
            foreach (Room room in currentRooms)
            {
                if (room.isAccessibleFromMainRoom)
                {
                    roomListB.Add(room);
                }
                else
                {
                    roomListA.Add(room);
                }
            }
        }
        else
        {
            roomListA = currentRooms;
            roomListB = currentRooms;
        }

        int bestDistance = 0;
        Coord bestTileA = new Coord();
        Coord bestTileB = new Coord();
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        bool possibleConnectionFound = false;

        foreach (Room roomA in roomListA)
        {
            if (!forceAccessibilityFromMainRoom)
            {
                possibleConnectionFound = false;
                if (roomA.connectedRooms.Count > 0)
                {
                    continue;
                }
            }

            foreach (Room roomB in roomListB)
            {
                if (roomA == roomB || roomA.IsConnected(roomB))
                {
                    continue;
                }
                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++)
                    {
                        Coord tileA = roomA.edgeTiles[tileIndexA];
                        Coord tileB = roomB.edgeTiles[tileIndexB];
                        int distanceBetweenRooms = (int)(Mathf.Pow(tileA.tileX - tileB.tileX, 2) + Mathf.Pow(tileA.tileY - tileB.tileY, 2));

                        if (distanceBetweenRooms < bestDistance || !possibleConnectionFound)
                        {
                            bestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }
            if (possibleConnectionFound && !forceAccessibilityFromMainRoom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }
        if (possibleConnectionFound && forceAccessibilityFromMainRoom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(currentRooms, true);
        }

        if (!forceAccessibilityFromMainRoom)
        {
            ConnectClosestRooms(currentRooms, true);
        }
    }

    void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB)
    {
        Room.ConnectRooms(roomA, roomB);

        List<Coord> line = GetLine(tileA, tileB);
        foreach (Coord c in line)
        {
            DrawCircle(c, pseudoRandom.Next(minimumPassageRadius, maximumPassageRadius));
        }

    }

    void DrawCircle(Coord c, int r)
    {
        for (int x = -r; x <= r; x++)
        {
            for (int y = -r; y <= r; y++)
            {
                if (x * x + y * y <= r * r)
                {
                    int realX = c.tileX + x;
                    int realY = c.tileY + y;
                    if (IsInMapRange(realX, realY))
                    {
                        map[realX, realY] = 0;
                    }
                }
            }
        }

    }

    List<Coord> GetLine(Coord from, Coord to)
    {
        List<Coord> line = new List<Coord>();
        int x = from.tileX;
        int y = from.tileY;
        int dx = to.tileX - from.tileX;
        int dy = to.tileY - from.tileY;

        bool inverted = false;

        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if (longest < shortest)
        {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;
        for (int i = 0; i < longest; i++)
        {
            line.Add(new Coord(x, y));
            if (inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;

            if (gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }
        return line;
    }

    Vector3 CoordToWorldPoint(Coord tile)
    {
        return new Vector3(-width / 2 + .5f + tile.tileX, 2, -height / 2 + .5f + tile.tileY);
    }

    List<List<Coord>> GetRegions(int tileType)
    {
        List<List<Coord>> regions = new List<List<Coord>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Coord> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);
                    foreach (Coord tile in newRegion)
                    {
                        mapFlags[tile.tileX, tile.tileY] = 1;
                    }
                }
            }
        }

        return regions;
    }

    List<Coord> GetRegionTiles(int startX, int startY)
    {
        List<Coord> tiles = new List<Coord>();
        int[,] mapFlags = new int[width, height];
        int tileType = map[startX, startY];

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(new Coord(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Coord tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
            {
                for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                {
                    if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Coord(x, y));
                        }
                    }
                }
            }
        }

        return tiles;
    }

    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    void RandomFillMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomMapFill) ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                {
                    map[x, y] = 1;
                }
                else if (neighbourWallTiles < 4)
                {
                    map[x, y] = 0;
                }
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }
        return wallCount;
    }

    public class Coord
    {
        public int tileX;
        public int tileY;

        public Coord()
        {

        }
        public Coord(int x, int y)
        {
            tileX = x;
            tileY = y;
        }
    }


    class Room : IComparable<Room>
    {
        public List<Coord> tiles;
        public List<Coord> edgeTiles;
        public List<Coord> interiorTiles;
        private HashSet<Coord> edgeTilesCheck;
        private HashSet<Coord> interiorTilesCheck;
        public List<Room> connectedRooms;
        public int roomSize;
        public bool isAccessibleFromMainRoom;
        public bool isMainRoom;

        public Room()
        {
        }

        public Room(List<Coord> roomTiles, int[,] map)
        {
            tiles = roomTiles;
            roomSize = tiles.Count;
            connectedRooms = new List<Room>();
            edgeTiles = new List<Coord>();
            interiorTiles = new List<Coord>();
            edgeTilesCheck = new HashSet<Coord>();
            interiorTilesCheck = new HashSet<Coord>();
            foreach (Coord tile in tiles)
            {
                for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
                {
                    for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                    {
                        if (x == tile.tileX || y == tile.tileY)
                        {
                            if (map[x, y] == 1)
                            {
                                if (!edgeTilesCheck.Contains(tile))
                                {
                                    edgeTiles.Add(tile);
                                    edgeTilesCheck.Add(tile);
                                }
                            }
                            else
                            {
                                if (!interiorTilesCheck.Contains(tile))
                                {
                                    interiorTiles.Add(tile);
                                    interiorTilesCheck.Add(tile);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SetAccessibleFromMainRoom()
        {
            if (!isAccessibleFromMainRoom)
            {
                isAccessibleFromMainRoom = true;
                foreach (Room connectedRoom in connectedRooms)
                {
                    connectedRoom.SetAccessibleFromMainRoom();
                }
            }
        }

        public static void ConnectRooms(Room roomA, Room roomB)
        {
            if (roomA.isAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if (roomB.isAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }

            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }

        public Coord getRandomTile(string seed, int[,] map, bool borderTile = false)
        {

            Coord[] tilesArray = (borderTile) ? edgeTiles.ToArray() : interiorTiles.ToArray();

            Coord tile = tilesArray[pseudoRandom.Next(0, tilesArray.Length)];

            return tile;
        }

        public int CompareTo(Room otherRoom)
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }

        public void AdjustForBorder(int xBorder, int yBorder)
        {
            foreach (Coord tile in tiles)
            {
                tile.tileX += xBorder;
                tile.tileY += yBorder;
            }
            foreach (Coord tile in edgeTiles)
            {
                tile.tileX += xBorder;
                tile.tileY += yBorder;
            }
            foreach (Coord tile in interiorTiles)
            {
                tile.tileX += xBorder;
                tile.tileY += yBorder;
            }
            foreach (Coord tile in edgeTilesCheck)
            {
                tile.tileX += xBorder;
                tile.tileY += yBorder;
            }
            foreach (Coord tile in interiorTilesCheck)
            {
                tile.tileX += xBorder;
                tile.tileY += yBorder;
            }
        }

    } 
    #endregion

	private Coord playerStartTile;

	void InitializePlayer3D()
	{        
		camera2D.gameObject.SetActive(false);
		player2D.gameObject.SetActive(false);
		player3D.gameObject.SetActive(true);
		minimapCamera.gameObject.SetActive (true);
		UICanvas3D.gameObject.SetActive (true);
		UICanvas2D.gameObject.SetActive (false);
		roof.SetActive(true);
		player3D.transform.position = new Vector3(-width / 2 + .5f + playerStartTile.tileX * squareSize, -height / 2 + .5f + playerStartTile.tileY * squareSize,3.9f-player3D.gameObject.GetComponent<CapsuleCollider>().height/2);
	}

    void InitializePlayer()
    {        
		playerStartTile = mainRoom.getRandomTile(seed,map);

		camera2D.gameObject.SetActive(true);
		roof.SetActive(false);
		player3D.gameObject.SetActive(false);
		player2D.gameObject.SetActive(true);
		minimapCamera.gameObject.SetActive (false);
		UICanvas3D.gameObject.SetActive (false);
		UICanvas2D.gameObject.SetActive (true);
		player2D.transform.position = new Vector2(-width / 2 + .5f + playerStartTile.tileX * squareSize, -height / 2 + .5f + playerStartTile.tileY * squareSize);
    }

	List<Coord> enemiesStartPositions, treasuresStartPositions, bonusStartPositions, exitStartPosition;

	void InitializeRooms()
	{
		int currentEnemies = 0, currentTreasures = 0, currentBonuses = 0, currentExits = 0;
		enemiesStartPositions = new List<Coord>();
		treasuresStartPositions = new List<Coord>();
		bonusStartPositions = new List<Coord>();
		exitStartPosition = new List<Coord>();
		while (currentEnemies < numberEnemies || currentTreasures < numberTreasures || currentBonuses < numberBonuses || currentExits < numberExits)
		{
			foreach (Room room in allRooms)
			{				
				if (room.isMainRoom)
				{
					//Aqui ya esta el Player
				}
				else
				{
					if (pseudoRandom.Next(1, 100) <= enemyChance)
					{
						if (currentEnemies < numberEnemies)
						{
							int randomEnemyIndex = pseudoRandom.Next(0, enemies2D.Length);
							Coord enemyTile = room.getRandomTile(seed,map);
							Enemy2D newEnemy = Instantiate(enemies2D[randomEnemyIndex]);
							newEnemy.transform.position = new Vector2(-width / 2 + .5f + enemyTile.tileX * squareSize, -height / 2 + .5f + enemyTile.tileY * squareSize);
							enemiesStartPositions.Add (enemyTile);
							currentEnemies++;
						}
					}
					if (pseudoRandom.Next(1, 100) <= treasureChance)
					{
						if (currentTreasures < numberTreasures)
						{
							int randomTreasureIndex = pseudoRandom.Next(0, treasures2D.Length);
							Coord treasureTile = room.getRandomTile(seed,map);
							Treasure2D newTreasure = Instantiate(treasures2D[randomTreasureIndex]);
							newTreasure.transform.position = new Vector2(-width / 2 + .5f + treasureTile.tileX * squareSize, -height / 2 + .5f + treasureTile.tileY * squareSize);
							treasuresStartPositions.Add (treasureTile);
							currentTreasures++;
						}
					}
					if (pseudoRandom.Next(1, 100) <= bonusChance)
					{
						if (currentBonuses < numberBonuses)
						{
							int randomBonusIndex = pseudoRandom.Next(0, bonuses2D.Length);
							Coord bonusTile = room.getRandomTile(seed,map);
							Bonus2D newBonus = Instantiate(bonuses2D[randomBonusIndex]);
							newBonus.transform.position = new Vector3(-width / 2 + .5f + bonusTile.tileX * squareSize, -height / 2 + .5f + bonusTile.tileY * squareSize, 4);;
							bonusStartPositions.Add (bonusTile);
							currentBonuses++;
						}
					}
					if (pseudoRandom.Next(1, 100) <= exitChance)
					{
						if (currentExits < numberExits)
						{
							Coord exitTile = room.getRandomTile(seed, map);
							Exit2D exit = Instantiate(exit2D);
							exit.transform.position = new Vector2(-width / 2 + .5f + exitTile.tileX * squareSize, -height / 2 + .5f + exitTile.tileY * squareSize);
							exitStartPosition.Add(exitTile);
							currentExits++;
						}
					}
					
				}
			}
		}
	}

    void InitializeRooms3D()
    {
		foreach (Coord tile in enemiesStartPositions) {
			//Añadir enemigo 3D
			int randomEnemyIndex = pseudoRandom.Next(0, enemies3D.Length);
			Enemy3D newEnemy = Instantiate(enemies3D[randomEnemyIndex]);
			newEnemy.transform.position = new Vector3(-width / 2 + .5f + tile.tileX * squareSize, -height / 2 + .5f + tile.tileY * squareSize,4);
		}
		foreach (Coord tile in treasuresStartPositions) {
			//Añadir tesoro 3D
			int randomTreasureIndex = pseudoRandom.Next(0, treasures3D.Length);
			Treasure3D newTreasure = Instantiate(treasures3D[randomTreasureIndex]);
			newTreasure.transform.position = new Vector3(-width / 2 + .5f + tile.tileX * squareSize, -height / 2 + .5f + tile.tileY * squareSize,4); 
		}
		foreach (Coord tile in bonusStartPositions) {
			//Añadir bonus 3D
			int randomBonusIndex = pseudoRandom.Next(0, bonuses3D.Length);
			Bonus3D newBonus = Instantiate(bonuses3D[randomBonusIndex]);
			newBonus.transform.position = new Vector3(-width / 2 + .5f + tile.tileX * squareSize, -height / 2 + .5f + tile.tileY * squareSize, 4); 
		}
		foreach (Coord tile in exitStartPosition) {
			//Añadir exit 3D
			Exit3D exit = Instantiate(exit3D);
			exit.transform.position = new Vector3(-width / 2 + .5f + tile.tileX * squareSize, -height / 2 + .5f + tile.tileY * squareSize, 4);
		}
    }

    public void EndLevel()
    {
        this.level++;
        levelTextUI.text = level.ToString();
		if (this.level % 2 == 0) {
			this.meshGenerator.is2D = false;
			Application.LoadLevel("Scene 3D");
		} else {
			this.meshGenerator.is2D = true;
        	Application.LoadLevel("Scene 2D");
		}
    }

    public void GameOver()
    {
        Destroy(player2D.gameObject);
		Destroy(player3D.gameObject);
        Destroy(UICanvas2D.gameObject);
        Destroy(mainCamera.gameObject);
        Destroy(this.gameObject);
        Application.LoadLevel("GameOver");
    }
}
