﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player2D : MonoBehaviour {

    Rigidbody2D _rigidbody;
    Vector2 velocity;
    public float maxSpeed = 10;
	public float acceleration = 1f;
	public float currentSpeed = 0f;

	public Animator animator;
	public SkinnedMeshRenderer meshRenderer;

    public int health;
    public int maxHealth = 100;
    public int treasure = 0;
    public float energy;
    public float maxEnergy = 300;

    public Image healthBar;
    public Image energyBar;
    public Text treasureValue;

    private float enemyCollisionTimeOut = 0f;
    public float enemyTimeoutSeconds = 2f;
    public float hitTimeoutSeconds = 0.2f;
    public Material normalColor;
    public Material hitColor;
    public Material timeOutColor;

    public MapGenerator mapGenerator;
    public float damageToEnergyMultiplier = 0.3f;

	private Vector2 lastInput;

    void Awake()
    {
        if (mapGenerator.level == 0)
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            health = maxHealth;
            energy = maxEnergy;
        }
    }
	
    void Start () {
        if (mapGenerator.level == 0)
        {

        }
        else
        {
            UpdateHealth(0);
            UpdateTreasure(0);
        }
	}
	
	// Update is called once per frame
	void Update () {
		float inputHorizontal =Input.GetAxisRaw("Horizontal");
		float inputVertical =Input.GetAxisRaw("Vertical");

		if (inputHorizontal != 0 || inputVertical != 0) {
			animator.SetBool("isMoving",true);
			if (currentSpeed < maxSpeed) {
				currentSpeed += acceleration * Time.deltaTime;
				if (currentSpeed > maxSpeed) {
					currentSpeed = maxSpeed;
				}
				animator.SetFloat("speed",0.5f+1.5f*currentSpeed/maxSpeed);
			}
			lastInput =new Vector2(inputHorizontal, inputVertical).normalized;
			velocity = lastInput  * currentSpeed;
		} else {
			if (currentSpeed > 0) {
				currentSpeed -= acceleration * acceleration *Time.deltaTime;
				if (currentSpeed < 0) {
					currentSpeed = 0;
					animator.SetBool("isMoving",false);
				}
				animator.SetFloat("speed",0.5f+1.5f*currentSpeed/maxSpeed);
			}
			velocity = lastInput * currentSpeed;
		}


        

		if (enemyCollisionTimeOut > 0)
        {
            enemyCollisionTimeOut -= Time.deltaTime;
            if (enemyCollisionTimeOut <= enemyTimeoutSeconds - hitTimeoutSeconds)
                ChangeColor(timeOutColor);
            if (enemyCollisionTimeOut <= 0)
            {
                enemyCollisionTimeOut = 0;
                ChangeColor(normalColor);
            }
        }
	}

    void FixedUpdate()
    {
        Vector2 impulse = velocity * Time.fixedDeltaTime;
        _rigidbody.MovePosition(_rigidbody.position + impulse);

        UpdateEnergy(-impulse.magnitude * damageToEnergyMultiplier);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (enemyCollisionTimeOut == 0)
            {
                enemyCollisionTimeOut += enemyTimeoutSeconds;
                Enemy2D enemy = collision.gameObject.GetComponent<Enemy2D>();
                ReceiveDamage(enemy.getDamage());
                Instantiate(enemy);
                ChangeColor(hitColor);
            }
        }
        else if (collision.gameObject.tag == "Treasure")
        {
            Treasure2D treasure = collision.gameObject.GetComponent<Treasure2D>();
            CatchTreasure(treasure.getTreasureValue());
            Destroy(treasure.gameObject);
        }
        else
        {

        }
    }

    void ChangeColor(Material newMaterial)
    {
        Material[] mats = meshRenderer.materials;
        mats[0] = newMaterial;
		meshRenderer.materials = mats;
    }

    void ReceiveDamage(int damage)
    {
        UpdateHealth(-damage);
        UpdateEnergy(-damage / 10);
    }

    void CatchTreasure(int treasureValue)
    {
        UpdateTreasure(treasureValue);
    }

    void UpdateHealth(int variation)
    {
        health += variation;
        if (health < 0) {
            health = 0;
            PlayerDie();
        }
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        healthBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 100* health/maxHealth);
    }

    void UpdateEnergy(float variation)
    {
        energy += variation;
        if (energy < 0)
        {
            energy = 0;
            PlayerDie();
        }
        if (energy > maxEnergy)
        {
            energy = maxEnergy;
        }
		if (energyBar != null)
        	energyBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 100* energy/maxEnergy);
    }

    void UpdateTreasure(int variation)
    {
        treasure += variation;
        treasureValue.text = treasure.ToString() + "€";
    }

    void PlayerDie()
    {
        mapGenerator.GameOver();
    }

    public void Recharge(float rechargedValue)
    {
        UpdateEnergy(rechargedValue);
    }
}
