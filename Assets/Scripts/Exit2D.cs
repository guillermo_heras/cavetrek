﻿using UnityEngine;
using System.Collections;

public class Exit2D : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject.FindObjectOfType<MapGenerator>().EndLevel();
        }
    }
}
