﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy2D : MonoBehaviour {

    Rigidbody2D _rigidbody;

    Vector2 newVelocity;
    public float impulseSpeed = 30;
    public float minimumVelocity = 1f;

    public int damage = 10;

    public float sightDistance = 10f;
    public CircleCollider2D sight;

    private System.Random pseudoRandom;

    public LayerMask sightLayerMask = -1;

    private bool playerSeen = false;
    private bool playerClose = false;
    private float timeOfPlayerClose;
    public float timeBetweenJumps = 0.3f;
    private Vector2 playerDirection;
    private bool wallReached;
    private Vector2 wallJumpAngle;
    private Vector2 lastDirection;
    private bool newImpulseToAdd = false;
    private Player2D player2D;

	// Use this for initialization
	void Start () {
        _rigidbody = GetComponent<Rigidbody2D>();
        player2D = GameObject.FindObjectOfType<Player2D>();
        pseudoRandom = new System.Random(DateTime.Now.ToLongTimeString().GetHashCode());
        sight.radius = sightDistance;
        float xVelocity = (float)pseudoRandom.NextDouble();
        if (pseudoRandom.Next(0, 100) <= 50)
            xVelocity *= -1;
        float yVelocity = (float)pseudoRandom.NextDouble();
        if (pseudoRandom.Next(0, 100) <= 50)
            yVelocity *= -1;
        lastDirection = new Vector2(xVelocity, yVelocity).normalized;
	}
	
	// Update is called once per frame
	void Update () {
        if (playerSeen)
        {
            newVelocity = playerDirection.normalized * impulseSpeed;
            newImpulseToAdd = true;
        }
        
        if (playerClose)
        {
            timeOfPlayerClose -= Time.deltaTime;
            if (timeOfPlayerClose <= 0.05f)
            {
                playerSeen = false;
                if (player2D != null)
                    PlayerSeen(player2D.gameObject);
            }
        }
        
        if (_rigidbody.velocity.magnitude <= minimumVelocity)
        {/*
            float xVelocity = (float)pseudoRandom.NextDouble()/10;
            if (pseudoRandom.Next(0, 100) <= 50)
                xVelocity *= -1;
            float yVelocity = (float)pseudoRandom.NextDouble()/10;
            if (pseudoRandom.Next(0, 100) <= 50)
                yVelocity *= -1;

            Vector2 newDirection = new Vector2(lastDirection.x + lastDirection.x * xVelocity, lastDirection.y + lastDirection.y*yVelocity);

            newVelocity = newDirection.normalized * impulseSpeed;*/
            newVelocity = lastDirection * impulseSpeed;
            newImpulseToAdd = true;
        }
	}

    void PlayerSeen(GameObject other)
    {
        Vector2 direction = other.transform.position - transform.position;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, sightDistance * 3, sightLayerMask);
        if (hit.collider != null && hit.collider.gameObject.tag == "Player")
        {
            playerDirection = direction;
            playerSeen = true;
            timeOfPlayerClose = timeBetweenJumps;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Map")
        {
            wallReached = true;
            wallJumpAngle = collision.relativeVelocity;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerClose = true;
            PlayerSeen(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerClose = false;
            timeBetweenJumps = 0;
            PlayerSeen(other.gameObject);
        }
    }

    void FixedUpdate()
    {
        if (wallReached)
        {
            _rigidbody.AddForce(new Vector2(-wallJumpAngle.x, -wallJumpAngle.y));
            wallReached = false;
            lastDirection = _rigidbody.velocity.normalized;
        }
        if (newImpulseToAdd)
        {
            _rigidbody.AddForce(newVelocity);
            newImpulseToAdd = false;
            lastDirection = _rigidbody.velocity.normalized;
        }
    }

    public int getDamage()
    {
        return damage;
    }


}
